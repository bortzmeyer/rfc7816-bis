XML2RFC ?= xml2rfc

# Sources for all our drafts
SOURCES += draft-ietf-dnsop-rfc7816bis

# RULES

XML_FILES := $(patsubst %,%.xml,$(SOURCES))
TXT_FILES := $(patsubst %,%.txt,$(SOURCES))

all: $(TXT_FILES)

$(TXT_FILES): %.txt: %.xml
	@$(XML2RFC) -o $@ $<

clean:
	@rm $(TXT_FILES)
