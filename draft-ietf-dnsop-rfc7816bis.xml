<?xml version="1.0" encoding="utf-8"?>

<!DOCTYPE rfc SYSTEM "rfc2629.dtd" [
<!ENTITY rfc1034 SYSTEM "https://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.1034.xml">
<!ENTITY rfc1035 SYSTEM "https://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.1035.xml">
<!ENTITY rfc2119 SYSTEM "https://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.2119.xml">
<!ENTITY rfc2181 SYSTEM "https://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.2181.xml">
<!ENTITY rfc4033 SYSTEM "https://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.4033.xml">
<!ENTITY rfc5246 SYSTEM "https://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.5246.xml">
<!ENTITY rfc5936 SYSTEM "https://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.5936.xml">
<!ENTITY rfc6347 SYSTEM "https://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.6347.xml">
<!ENTITY rfc6672 SYSTEM "https://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.6672.xml">
<!ENTITY rfc6895 SYSTEM "https://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.6895.xml">
<!ENTITY rfc6973 SYSTEM "https://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.6973.xml">
<!ENTITY rfc7816 SYSTEM "https://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.7816.xml">
<!ENTITY rfc8020 SYSTEM "https://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.8020.xml">
<!ENTITY rfc8174 SYSTEM "https://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.8174.xml">
<!ENTITY rfc8198 SYSTEM "https://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.8198.xml">
<!ENTITY rfc8499 SYSTEM "https://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.8499.xml">
<!ENTITY rfc9076 SYSTEM "https://xml2rfc.tools.ietf.org/public/rfc/bibxml/reference.RFC.9076.xml">
]>

<rfc docName="draft-ietf-dnsop-rfc7816bis-11"
  category="std" ipr="trust200902" obsoletes="7816" consensus="yes">

<?rfc toc="yes"?>
<?rfc sortrefs="yes"?>
<?rfc compact="yes"?>
<?rfc subcompact="no"?>

<front>
<title abbrev="QNAME Minimisation">DNS Query Name Minimisation to Improve Privacy</title>

<author fullname="Stephane Bortzmeyer" initials="S." surname="Bortzmeyer">
<organization>AFNIC</organization>
<address>
<postal>
<street>1, rue Stephenson</street>
<code>78180</code>
<city>Montigny-le-Bretonneux</city>
<country>France</country>
</postal>
<phone>+33 1 39 30 83 46</phone>
<email>bortzmeyer+ietf@nic.fr</email>
<uri>https://www.afnic.fr/</uri>
</address>
</author>

<author fullname="Ralph Dolmans" initials="R." surname="Dolmans">
<organization>NLnet Labs</organization>
<address>
<email>ralph@nlnetlabs.nl</email>
</address>
</author>

<author fullname="Paul Hoffman" initials="P." surname="Hoffman">
<organization>ICANN</organization>
<address>
<email>paul.hoffman@icann.org</email>
</address>
</author>

<date />

<abstract>

<t>This document describes a technique called "QNAME minimisation" to improve
DNS privacy, where the DNS resolver no longer always sends the full
original QNAME and original QTYPE to the upstream name server. This document
obsoletes RFC 7816.</t>

</abstract>
</front>

<middle>

<section anchor="intro" title="Introduction and Background">

<t>The problem statement for this document is described in <xref target="RFC9076"/>.
This specific solution is not
intended to fully solve the DNS privacy problem; instead, it
should be viewed as one tool amongst many.</t>

<t>QNAME minimisation follows the principle explained in
Section 6.1 of <xref target="RFC6973"/>: the less data you send out,
the fewer privacy problems you have.</t>

<t>Before QNAME minimisation, when a resolver received the query "What is
the AAAA record for www.example.com?", it sent to the root (assuming
a resolver whose cache is empty) the very same question. Sending
the full QNAME to the authoritative name server was a tradition,
not a protocol requirement. In a conversation with one of the authors in
January 2015, Paul Mockapetris explained that this tradition comes
from a desire to optimise the number of requests, when the same
name server is authoritative for many zones in a given name
(something that was more common in the old days, where the same
name servers served .com and the root) or when the same
name server is both recursive and authoritative (something that is
strongly discouraged now). Whatever the merits of this choice at this
time, the DNS is quite different now.</t>

<t>QNAME minimisation is compatible with the current DNS system and
therefore can easily be deployed. Because it is only a change to
the way that the resolver operates, it does not change the DNS protocol itself.
The behaviour suggested here (minimising the
amount of data sent in QNAMEs from the resolver) is allowed by
Section 5.3.3 of <xref target="RFC1034"/> and
Section 7.2 of <xref target="RFC1035"/>.</t>

<section title="Experience From RFC 7816">

<t>This document obsoletes <xref target="RFC7816" />.
RFC 7816 was labelled "experimental", but ideas from it were widely
deployed since its publication.
Many resolver implementations now support QNAME minimisation. The lessons 
learned from implementing QNAME minimisation were used to create this new
revision.</t>

<t>Data from DNSThought <xref target="dnsthought-qnamemin" />,
Verisign <xref target="verisign-qnamemin" /> and APNIC <xref target="apnic-qnamemin"/>
shows that a large percentage of the resolvers deployed on the Internet already support
QNAME minimisation in some way.</t>

<t>Academic research has been performed on QNAME minimisation
<xref target="devries-qnamemin"/>. This
work shows that QNAME minimisation in relaxed mode causes almost no problems.
The paper recommends using the A QTYPE, and limiting the number of queries in
some way.
Some of the issues that the paper found are covered in <xref target="perf_cons"/>.</t>

</section>
  
<section title="Terminology">

<t>The terminology used in this document is defined in  <xref target="RFC8499"/>.</t>

<t>In this document, a "cold" cache is one that is empty, having literally no entries
in it. A "warm" cache is one that has some entries in it.</t>

<t>The key words “MUST”, “MUST NOT”, “REQUIRED”, “SHALL”, “SHALL NOT”,
“SHOULD”, “SHOULD NOT”, “RECOMMENDED”, “NOT RECOMMENDED”, “MAY”,
and “OPTIONAL” in this document are to be interpreted as described in
BCP 14 <xref target="RFC2119"/> <xref target="RFC8174"/> when, and only when, they
appear in all capitals, as shown here.</t>

</section>

</section>

<section anchor="qname-main" title="Description of QNAME Minimisation">

<t>The idea behind QNAME minimisation is to minimise the amount of
privacy-sensitive data sent from the DNS resolver to the authoritative
name server. This section describes how to do QNAME minimisation. The algorithm
is summarised in <xref target="zonecutalgo"/>.</t>

<t>When a resolver is not able to answer a query from cache it has
to send a query to an authoritative nameserver. Traditionally these
queries would contain the full QNAME and the original QTYPE as received
in the client query.</t>

<t>The full QNAME and original QTYPE are only needed at the nameserver that
is authoritative for the record requested by the client. All other nameservers
queried while resolving the query only need to receive enough of the QNAME to
be able to answer with a delegation. The QTYPE in these queries is not
relevant, as the nameserver is not able to authoritatively answer the records
the client is looking for. Sending the full QNAME and original QTYPE to
these nameservers therefore exposes more privacy-sensitive data than
necessary to resolve the client's request.</t>

<t>A resolver that implements QNAME minimisation obscures the QNAME and QTYPE in
queries directed to an authoritative nameserver that is not known to be
responsible for the original QNAME. These queries contain:

<list style="symbols">

<t>a QTYPE selected by the resolver to possibly obscure the original QTYPE</t>

<t>the QNAME that is the original QNAME, stripped to just one
label more than the longest matching domain name for which the
nameserver is known to be authoritative</t>

</list></t>

<section title="QTYPE Selection">
<t>Note that this document relaxes the recommendation in RFC 7816 to use the NS QTYPE to
hide the original QTYPE. Using the NS QTYPE is
still allowed. The authority of NS records lies at the child side. The parent
side of the delegation will answer using a referral, like it will do for queries
with other QTYPEs. Using the NS QTYPE therefore has no added value over other
QTYPEs.</t>

<t>The QTYPE to use while minimising queries can be any possible data type
(as defined in <xref target="RFC6895"/> Section 3.1) for which the authority always lies below the zone cut
(i.e. not DS, NSEC, NSEC3, OPT, TSIG, TKEY, ANY, MAILA, MAILB,
AXFR, and IXFR), as long as there is no relation between the incoming QTYPE and
the selection of the QTYPE to use while minimising.
Good candidates are to always use the "A" or "AAAA"
QTYPE because these are the least likely to raise issues in DNS
software and middleboxes that do not properly support all QTYPEs.
QTYPE=A or QTYPE=AAAA queries will also blend into traffic from non-minimising
resolvers, making it in some cases harder to observe that the
resolver is using QNAME minimisation.  Using a QTYPE that occurs
most in incoming queries will slightly reduce the number of queries,
as there is no extra check needed for delegations on non-apex
records. 
</t>
</section>

<section title="QNAME Selection">
<t>The minimising resolver works perfectly when it knows the zone cut
(zone cuts are described in Section 6 of 
<xref target="RFC2181"/>).  But zone cuts do not
necessarily exist at every label boundary.  In the name
www.foo.bar.example, it is possible that there is a zone cut between
"foo" and "bar" but not between "bar" and "example". So, assuming
that the resolver already knows the name servers of example, when it
receives the query "What is the AAAA record of www.foo.bar.example?",
it does not always know where the zone cut will be.  To find the
zone cut, it will query the example name servers for a record for
bar.example. It will get a non-referral answer, it has to query the
example name servers again with one more label, and so on.
(<xref target="zonecutalgo"/> describes this algorithm in deeper
detail.)</t>
</section>

<section anchor="number-of-queries" title="Limit Number of Queries">

<t>When using QNAME minimisation, the number of labels in the received QNAME can
influence the number of queries sent from the resolver. This opens an attack
vector and can decrease performance. Resolvers supporting QNAME minimisation
MUST implement a mechanism to limit the number of outgoing queries per user
request.</t>

<t>Take for example an incoming QNAME with many labels, like
www.host.group.department.example.com, where
host.group.department.example.com is hosted on example.com's
name servers.
(Such deep domains are especially common under ip6.arpa.)
Assume a resolver that knows only the
name servers of example.com. Without QNAME minimisation, it would
send these example.com name servers a query for
www.host.group.department.example.com and immediately get a
specific referral or an answer, without the need for more queries
to probe for the zone cut. For such a name, a cold resolver with
QNAME minimisation will send more queries, one per label. Once the cache is
warm, there will be less difference with a traditional resolver.
Testing of this is described in <xref target="Huque-QNAME-Min"/>.
</t>

<t>The behaviour of sending multiple queries can be exploited by sending queries with a large number of
labels in the QNAME that will be answered using a wildcard record. Take for
example a record for *.example.com, hosted on example.com's name servers. An
incoming query containing a QNAME with more than 100 labels, ending in
example.com, will result in a query per label. By using random labels, the
attacker can bypass the cache and always require the resolver to send many
queries upstream. Note that <xref target="RFC8198"/> can limit this attack in
some cases.</t>

<t>One mechanism that MAY be used to reduce this attack vector is by appending  more than one
label per iteration for QNAMEs with a large number of labels. To do this, a
maximum number of QNAME minimisation iterations MUST be selected
(MAX_MINIMISE_COUNT); a RECOMMENDED value is 10. Optionally, a value for the number of
queries that should only have one label appended MAY be selected
(MINIMISE_ONE_LAB), a good value is 4. The assumption here is that the number
of labels on delegations higher in the hierarchy are rather small, therefore
not exposing too many labels early on has the most privacy benefit.</t>

<t>Another potential, optional mechanism for limiting the number of queries is to assume that labels that begin with
an underscore (_) character do not represent privacy-relevant administrative boundaries.
For example, if the QNAME is "_25._tcp.mail.example.org" and the algorithm has
already searched for "mail.example.org", the next query can be for all the underscore-prefixed
names together, namely "_25._tcp.mail.example.org".</t>

<t>When a resolver needs to send out a query, it will look for the closest known
delegation point in its cache. The number of not yet exposed labels is
the difference between this closest nameserver and the incoming QNAME. The
first MINIMISE_ONE_LAB labels will be handled as described in
<xref target="qname-main"/>. The number of labels that are still not exposed now
need to be divided proportionally over the remaining iterations
(MAX_MINIMISE_COUNT - MINIMISE_ONE_LAB). If the not yet exposed labels can not be equally divided over the remaining iterations, the remainder of the division should 
be added to the last iterations. For example, when resolving a QNAME with 18
labels with MAX_MINIMISE_COUNT set to 10 and MINIMISE_ONE_LAB set to 4, the number of labels added per iteration are: 1,1,1,1,2,2,2,2,3,3.</t>

</section>

<section title="Stub and Forwarding Resolvers">
<t>Stub and forwarding resolvers MAY implement QNAME minimisation. Minimising
queries that will be sent to an upstream resolver does not help in hiding data
from the upstream resolver because all information will end up there anyway. It
might, however, limit the data exposure between the upstream resolver and the
authoritative nameserver in the situation where the upstream resolver does not
support QNAME minimisation. Using QNAME minimisation in a stub or forwarding
resolvers that does not have a mechanism to find and cache zone cuts will drastically increase
the number of outgoing queries.</t>
</section>

</section>

<section anchor="zonecutalgo" title="Algorithm to Perform QNAME Minimisation">

<t>This algorithm performs name resolution with QNAME minimisation in
the presence of zone cuts that are not yet known.</t>

<t>Although a validating resolver already has the logic to find the
zone cuts, implementers of resolvers may want to use
this algorithm to locate the zone cuts.

<list style="hanging" hangIndent="4">

<t hangText="(0)">If the query can be answered from the cache, do so;
otherwise, iterate as follows:</t>

<t hangText="(1)">Get the closest delegation point that can be used for
the original QNAME from the cache.
<list style="hanging" hangIndent="5">

<t hangText="(1a)">
For queries with a QTYPE for which the authority only lies at the parent side (like QTYPE=DS) this is the NS RRset with the owner matching the most
labels with QNAME stripped by one label. QNAME will be a subdomain of
(but not equal to) this NS RRset.  Call this ANCESTOR.</t>

<t hangText="(1b)">
For queries with other original QTYPEs this is the NS RRset with the owner
matching the most labels with QNAME. QNAME will be equal to or a
subdomain of this NS RRset.  Call this ANCESTOR.</t>
</list></t>

<t hangText="(2)">Initialise CHILD to the same as ANCESTOR.</t>

<t hangText="(3)">If CHILD is the same as QNAME, or if CHILD is one
label shorter than QNAME and the original QTYPE can only be at the parent side (like QTYPE=DS), resolve the
original query as normal starting from ANCESTOR's name servers. Start over from step 0 if new names need to be resolved as result of this answer, for example when the answer contains a CNAME or DNAME <xref target="RFC6672"/> record.</t>

<t hangText="(4)">Otherwise, update the value of CHILD by adding the next relevant label or labels from QNAME to the start
of CHILD. The number of labels to add is discussed in <xref target="number-of-queries"/>.</t>

<t hangText="(5)">Look for a cache entry for the RRset at CHILD with the original
QTYPE. If the cached response code is NXDOMAIN and the resolver has support for <xref target="RFC8020"/>,
the NXDOMAIN can be used in response to the original query, and stop. If the
cached response code is NOERROR (including NODATA), go back to step 3.
If the cached response code is NXDOMAIN and the resolver does not support
RFC 8020, go back to step 3.</t>

<t hangText="(6)">Query for CHILD with the selected QTYPE using one of ANCESTOR's
name servers. The response can be:

<list style="hanging" hangIndent="5">

<t hangText="(6a)">A referral. Cache the NS RRset from the authority
section, and go back to step 1.</t>

<t hangText="(6b)">A DNAME response. Proceed as if a DNAME is received for the
original query. Start over from step 0 to resolve the new name based on the
DNAME target.</t>

<t hangText="(6c)">All other NOERROR answers (including NODATA).
Cache this answer. Regardless of the answered RRset type, including CNAMEs,
continue with the algorithm from step 3 by building the original QNAME.</t>

<t hangText="(6d)">An NXDOMAIN response.
If the resolver supports RFC8020, return an NXDOMAIN response to the original query and stop.
If the resolver does not support RFC8020, go to step 3.</t>

<t hangText="(6e)"> A timeout or response with another RCODE. The implementation
may choose to retry step (6) with a different ANCESTOR name server.</t>

</list></t>

</list></t>

</section>

<section title="QNAME Minimisation Examples">

<t>As a first example, assume that a resolver receives a request to resolve
foo.bar.baz.example. Assume that the resolver already knows that
ns1.nic.example is authoritative for .example, and that the resolver does
not know a more specific authoritative name server. It will send the
query with QNAME=baz.example and the QTYPE selected to hide the original
QTYPE to ns1.nic.example.</t>

<t>The following are more detailed examples of other queries with QNAME
minimisation, using other names and authoritative servers:</t>

<t>Cold cache, traditional resolution algorithm without QNAME minimisation,
request for MX record of a.b.example.org:

<figure><artwork>
QTYPE   QNAME           TARGET                 NOTE
MX      a.b.example.org root nameserver
MX      a.b.example.org org nameserver
MX      a.b.example.org example.org nameserver
</artwork></figure></t>

<t>Cold cache, with QNAME minimisation, request for MX record of
a.b.example.org, using the A QTYPE to hide the original QTYPE:

<figure><artwork>
QTYPE   QNAME           TARGET                 NOTE
A       org             root nameserver
A       example.org     org nameserver
A       b.example.org   example.org nameserver 
A       a.b.example.org example.org nameserver "a" may be delegated
MX      a.b.example.org example.org nameserver
</artwork></figure>

Note that in above example one query would have been saved if the incoming QTYPE
was the same as the QTYPE selected by the resolver to hide the
original QTYPE. Only one query for a.b.example.org would have been needed if
the original QTYPE would have been A. Using the most used QTYPE to hide the
original QTYPE therefore slightly reduces the number of outgoing queries compared to using any other QTYPE to hide the original QTYPE.
</t>

<t>
Warm cache with only org delegation known, (example.org's NS RRset is
not known), request for MX record of
a.b.example.org, using A QTYPE to hide the original QTYPE:

<figure><artwork>
QTYPE   QNAME           TARGET                 NOTE
A       example.org     org nameserver
A       b.example.org   example.org nameserver
A       a.b.example.org example.org nameserver "a" may be delegated
MX      a.b.example.org example.org nameserver
</artwork></figure></t>

</section>

<section title="Performance Considerations" anchor="perf_cons">

<t>The main goal of QNAME minimisation is to improve privacy by
sending less data. However, it may have other advantages. For
instance, if a resolver sends a root name server queries
for A.example followed by B.example followed by C.example, the result
will be three NXDOMAINs, since .example does not exist in the root
zone. When using QNAME minimisation, the resolver would send
only one question (for .example itself) to which they could answer
NXDOMAIN. The resolver can cache this answer and use it as to prove that
nothing below .example exists (<xref target="RFC8020"/>). A resolver now knows
a priori that neither B.example nor C.example exist. Thus, in this common case,
the total number of upstream
queries under QNAME minimisation could counterintuitively be less
than the number of queries under the traditional iteration (as
described in the DNS standard).
</t>

<t>QNAME minimisation can increase the number of queries based on the incoming
QNAME. This is described in <xref target="number-of-queries"/>.
As described in <xref target="devries-qnamemin"/>, QNAME minimisation
both increases the number of DNS lookups by up to 26% and leads to up to 5% more failed lookups.
Filling the cache in a production resolver will soften that overhead.</t>

</section>

<section title="Security Considerations">

<t>QNAME minimisation's benefits are clear in the case where you want
to decrease exposure of the queried name to the authoritative name server. But minimising
the amount of data sent also, in part, addresses the case of a wire
sniffer as well as the case of privacy invasion by the authoritative name
servers. (Encryption is of course a better defense against wire
sniffers, but, unlike QNAME minimisation, it changes the protocol and
cannot be deployed unilaterally. Also, the effect of QNAME
minimisation on wire sniffers depends on whether the sniffer is on
the DNS path.)</t>

<t>QNAME minimisation offers no protection against the recursive
resolver, which still sees the full request coming from the stub
resolver.</t>

<t>A resolver using QNAME minimisation can possibly be used to cause
a query storm to be sent to servers when resolving queries containing a QNAME
with a large number of labels, as described in
<xref target="number-of-queries"/>. That section proposes methods to
significantly dampen the effects of such attacks.</t>

</section>

</middle>

<back>

<references title='Normative References'>
&rfc1034;
&rfc1035;
&rfc2119;
&rfc6973;
&rfc8174;
&rfc8499;
</references>

<references title='Informative References'>
&rfc2181;
&rfc6672;
&rfc6895;
&rfc7816;
&rfc8020;
&rfc8198;
&rfc9076;

<reference anchor="Huque-QNAME-Min" target="https://indico.dns-oarc.net/event/21/contribution/9">
<front>
<title>Query name minimization and authoritative server behavior</title>
<author fullname="Shumon Huque" initials="S." surname="Huque"/>
<date month="May" year="2015"/>
</front>
</reference>

<reference anchor="dnsthought-qnamemin"
        target="https://dnsthought.nlnetlabs.nl/#qnamemin">
  <front>
    <title>DNSThought QNAME minimisation results. Using Atlas probes</title>
    <author fullname="" initials=""
        surname=""/>
    <date month="March" year="2020"/>
  </front>
</reference>

<reference anchor="devries-qnamemin"
	target="https://nlnetlabs.nl/downloads/publications/devries2019.pdf">
  <front>
	  <title>A First Look at QNAME Minimization in the Domain Name System</title>
    <author fullname="de Vries et al." initials=""
        surname=""/>
    <date month="March" year="2019"/>
  </front>
</reference>

<reference anchor="verisign-qnamemin"
        target="https://blog.verisign.com/security/maximizing-qname-minimization-a-new-chapter-in-dns-protocol-evolution/">
  <front>
    <title>Maximizing Qname Minimization: A New Chapter in DNS Protocol Evolution</title>
    <author fullname="Matt Thomas" initials="M."
        surname="Thomas"/>
    <date month="September" year="2020"/>
  </front>
</reference>
  
<reference anchor="apnic-qnamemin"
        target="https://indico.dns-oarc.net/event/34/contributions/787/attachments/777/1326/2020-09-28-oarc33-qname-minimisation.pdf">
  <front>
    <title>Measuring Query Name Minimization</title>
    <author fullname="Geoff Huston" initials="G."
        surname="Huston"/>
    <author fullname="Joao Damas" initials="J."
        surname="Damas"/>
    <date month="September" year="2020"/>
  </front>
</reference>
  
</references>

<section title="Acknowledgments" numbered="no">

<t>The acknowledgements from  RFC 7816 apply here.
In addition, many participants from the DNSOP Working Group helped
with proposals for simplification, clarification, and general
editorial help.
</t>
</section>

<section title="Changes from RFC 7816" numbered="no">

<t>Changed in -07<list style="symbols">
<t>Stopped using the term "aggressive" for the method described</t>
<t>Clarified some terminology</t>
<t>More reorganization</t>
</list></t>

<t>Changed in -06<list style="symbols">
<t>Removed lots of text from when this was experimental</t>
<t>Lots of reorganization</t>
</list></t>


<t>Changed in -04<list style="symbols">
<t>Start structure for implementation section</t>
<t>Add clarification why the used QTYPE does not matter</t>
<t>Make algorithm DS QTYPE compatible</t>
</list></t>

<t>Changed in -03<list style="symbols">

<t>Drop recommendation to use the NS QTYPE to hide the incoming QTYPE</t>

<t>Describe DoS attach vector for QNAME with large number of labels, and propose
a mitigation.</t>

<t>Simplify examples and change qname to a.b.example.com to show the change in
number of queries.</t>

</list></t>

<t>Changed in -00, -01, and -02<list style="symbols">
<t>Made changes to deal with errata #4644</t>

<t>Changed status to be on standards track</t>

<t>Major reorganization</t>

</list></t>
</section>

</back>

</rfc>
